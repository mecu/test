## move1
This only need to be run once per repository by someone with owner permissions.

`GITLAB_PROJECT=PROJECTID GITLAB_TOKEN=TOKEN ./move1`

## move2
This needs to be run by everyone who previously cloned the repo.
`./move2`
